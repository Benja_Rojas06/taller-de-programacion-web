# Propuesta Solución Laboratorio 1

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Taller de Programación Web

## Construcción del Programa:

- Sistema operativo: Microsoft Windows 10.
- Visual Studio Code: Editor de texto utilizado para escribir el código del programa.
- Python 3.7.0: Lenguaje utilizado para resolver la problemática.

## Puntos a considerar:

- Se utilizan diversas hojas de estilo.
- Se escribe un código tabulado.
- Se utilizan imágenes, videos y gifs en las diferentes páginas creadas.

## Realizar revisión de History -> Browse Directory:

- Favor revisar el historial de los commits realizados. Accidentalmente borré los archivos que se encontraban en mi repositorio y, por ende, los volví a subir. Con esto han quedado "fuera de plazo" de envío el Laboratorio 1 y Guía 1. Sin embargo, en el historial se puede apreciar que los últimos cambios realizados sí entraban en el tiempo estipulado para el desarrollo de los mismos.