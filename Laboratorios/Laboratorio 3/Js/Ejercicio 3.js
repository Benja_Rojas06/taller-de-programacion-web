// Creación de las variables y asignación de los números ingresados.
var num1 = prompt("Ingrese un número: ");
var num2 = prompt("Ingrese otro número: ");

// Se determina cual de los dos números es mayor, si el primero es mayor 
// se escribe en pantalla, sino, el segundo.
if(num1 > num2){
    document.write("El número mayor es: ", num1);
    document.write("<br>Porque" + num1 + " > " + num2);
} else{
    document.write("El número mayor es: ", num2);
    document.write("<br>Porque " + num2 + " > " + num1);
}