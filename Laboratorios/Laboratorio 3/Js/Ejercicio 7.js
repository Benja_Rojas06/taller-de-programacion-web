// Creación de la variable y asignación de la frase ingresada.
var frase = prompt("Ingrese una frase: ");
// Contador de vocales a.
var vocalA = 0;
// Cantidad total de letras.
var letras = frase.length;
var i;

document.write("Las vocales que aparecen en la frase ingresada son:<br>");

// Se recorre la frase transformando todas las letras a mayúsculas,
// si se encuentra una coincidencia con alguna vocal, entonces la muestra en pantalla.
for(i = 0; i < letras; i++){
    if(frase[i].toUpperCase() == "A" || frase[i].toUpperCase() == "E" || 
    frase[i].toUpperCase() == "I" || frase[i].toUpperCase() == "O" || 
    frase[i].toUpperCase() == "U"){
        document.write(frase[i].toUpperCase());
    }
}