// Creación de la variable y asignación de la frase ingresada.
var frase = prompt("Ingrese una frase: ");
// Contador de vocales a.
var vocalA = 0;
// Cantidad total de letras.
var letras = frase.length;
var i;

// Se recorre la frase transformando todas las letras a mayúsculas,
// si se encuentra una coincidencia con "A", se sumará 1 al contador de vocales A.
for(i = 0; i < letras; i++){
    if(frase[i].toUpperCase() == "A"){
        vocalA = vocalA + 1;
    }
}

// Se indica la cantidad total de vocales "A" existentes en la frase ingresada.
document.write("Hay un total de " + vocalA + " vocales 'A'");
