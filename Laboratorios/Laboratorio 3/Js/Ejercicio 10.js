// Creamos función para que se realice el "lanzamiento" de manera aleatoria.
function numero_random(){
    // Se genera un número aleatorio entre 1 y 6.
    var num_random = Math.floor((Math.random() * (6)) + 1);
    // Retornamos el número.
    return num_random;
}

// Función para que se muestre el dado según el número obtenido.
function lanzamiento(){
    // Se llama a la función para realizar la generación de un número aleatorio.
    var num_dado = numero_random();

    // Se evalúan todos los casos posibles, donde cada uno mostrará la imagen que le corresponda.
    if(num_dado == 1){
        // Dado 1.
        document.getElementById("foto").src = "../Fotos/Ejercicio 10/dado1.png";
    } else if(num_dado == 2){
        // Dado 2.
        document.getElementById("foto").src = "../Fotos/Ejercicio 10/dado2.png";
    } else if(num_dado == 3){
        // Dado 3.
        document.getElementById("foto").src = "../Fotos/Ejercicio 10/dado3.png";
    } else if(num_dado == 4){
        // Dado 4.
        document.getElementById("foto").src = "../Fotos/Ejercicio 10/dado4.png";
    } else if(num_dado == 5){
        // Dado 5.
        document.getElementById("foto").src = "../Fotos/Ejercicio 10/dado5.png";
    } else if(num_dado == 6){
        // Dado 6.
        document.getElementById("foto").src = "../Fotos/Ejercicio 10/dado6.png";
    }
}