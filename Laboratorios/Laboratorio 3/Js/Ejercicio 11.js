// Creamos un arreglo con todas las fotos que tenemos.
var arreglo_fotos = new Array("../Fotos/Ejercicio 11/foto1.jpg", "../Fotos/Ejercicio 11/foto2.jpg", "../Fotos/Ejercicio 11/foto3.jpg", 
                                "../Fotos/Ejercicio 11/foto4.jpg", "../Fotos/Ejercicio 11/foto5.jpg");

// Creamos un contador para ver la posición de las fotos.
// Así sabremos si nos encontramos en la foto 1, 2, 3, 4 o 5.
var posicion = 0;

// Función para visualizar la foto anterior.
function anterior(){
    // Siempre y cuando la posición se encuentre por sobre la foto 1,
    // se podrá retroceder a la anterior.
    if(posicion > 0){
        // Retrocedemos una posición.
        posicion = posicion - 1

        // Mostramos la foto actual actualizando la posición en la que nos encontramos.
        document.getElementById("foto").src = arreglo_fotos[posicion];
    }
}

// Función para visualizar la foto siguiente.
function siguiente(){
    // Siempre y cuando la posición se encuentre bajo la foto 5,
    // se podrá avanzar a la siguiente.
    if(posicion < 4){
        // Avanzamos una posición.
        posicion = posicion + 1

        // Mostramos la foto actual actualizando la posición en la que nos encontramos.
        document.getElementById("foto").src = arreglo_fotos[posicion];
    }
}