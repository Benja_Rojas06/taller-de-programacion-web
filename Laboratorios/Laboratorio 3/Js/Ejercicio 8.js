// Creación de la variable y asignación de la frase ingresada.
var frase = prompt("Ingrese una frase: ");
// Contadores de vocales.
var vocalA = 0;
var vocalE = 0;
var vocalI = 0;
var vocalO = 0;
var vocalU = 0;
// Cantidad total de letras.
var letras = frase.length;
var i;

// Se recorre la frase transformando todas las letras a mayúsculas,
// si se encuentra una coincidencia con "A", se sumará 1 al contador de vocales A 
// y así sucesivamente para el resto de las vocales.
for(i = 0; i < letras; i++){
    if(frase[i].toUpperCase() == "A"){
        vocalA = vocalA + 1;
    } else if(frase[i].toUpperCase() == "E"){
        vocalE = vocalE + 1;
    } else if(frase[i].toUpperCase() == "I"){
        vocalI = vocalI + 1;
    } else if(frase[i].toUpperCase() == "O"){
        vocalO = vocalO + 1;
    } else if(frase[i].toUpperCase() == "U"){
        vocalU = vocalU + 1;
    }
}

// Se indica la cantidad total de vocales, por cada tipo, existentes en la frase ingresada.
document.write("Hay un total de " + vocalA + " vocales 'A'");
document.write("<br>Hay un total de " + vocalE + " vocales 'E'");
document.write("<br>Hay un total de " + vocalI + " vocales 'I'");
document.write("<br>Hay un total de " + vocalO + " vocales 'O'");
document.write("<br>Hay un total de " + vocalU + " vocales 'U'");