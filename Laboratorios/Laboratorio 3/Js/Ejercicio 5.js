// Creación de la variable y asignación del número ingresado.
var num1 = prompt("Ingrese un número: ");

// Si el módulo de 2 es igual a 0, implicará que el número es par (divisible por 2), 
// sino, es impar (no divisible por 2 dando una cifra entera).
if(num1 %2 == 0){
    document.write("El número " + num1 + " sí es divisible por 2");
} else{
    document.write("El número " + num1 + " no es divisible por 2");
}