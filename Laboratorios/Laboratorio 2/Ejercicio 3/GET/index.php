<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title> Ejercicio 3 </title>
    <link rel="stylesheet" type="text/css" href="../Estilo/estilo.css">
</head>

<body>
    <div class="titulo">
        <h1> <u> Tabla de N x N con Método GET </u> </h1>
    </div>

    <!-- Creación de formulario que se llama a sí mismo mediante el método GET -->
    <form action = "../GET/index.php" method="GET">
        <!-- Se solicita el ingreso de la cantidad de filas -->
        <input type="number" min=1 placeholder="Ingrese cantidad de filas" name="cant_filas">
        <!-- Se solicita que se ingresen los colores mediante una paleta de colores -->
        <p class="texto"> <b> Color filas pares: </b>
        <input type="color" name ="colorA">
        </p>
        <p class="texto"> <b> Color filas impares: </b>
        <input type="color" name ="colorB">
        </p>
        <!-- Botón para la creación de la tabla -->
        <input type="submit" value ="Crear Tabla">
    </form>

    <div class="tabla" style="margin: 0 auto">
        <table>
            <?php
                // Inicializamos el contador en 1.
                $cont = 1;
                // Se corrobora que la cantidad de filas ingresadas no sea un valor nulo.
                if(isset($_GET['cant_filas'])){
                    $numeroFilas = $_GET['cant_filas'];

                    // Primer ciclo para crear las filas.
                    for($filas = 1; $filas <= $numeroFilas; $filas++){
                        // Se corrobora que el color "A" ingresado no sea nulo.
                        if(isset($_GET['colorA'])){
                            $colorSA = $_GET['colorA'];

                            // Se imprime el inicio de la fila.
                            // Cálculo de si la fila es impar; colorea según lo indicado por el usuario en color "A".
                            if($filas % 2 == 1){
                                echo"<tr style='background-color: $colorSA'>";
                            }else{
                                // Se corrobora que el color "B" ingresado no sea nulo.
                                if(isset($_GET['colorB'])){
                                    $colorSB = $_GET['colorB'];
                                    echo"<tr style='background-color: $colorSB'>";
                                }
                            }
                            // Segundo ciclo para rellenar las columnas.
                            for($cols = 1; $cols <= $numeroFilas;  $cols++){
                                // Se imprime la columna y el número correspondiente.
                                echo"<td>".$cont."</td>";

                                // Incrementamos el contador.
                                $cont++;
                            }
                        }
                        // Se imprime el cierre de la fila.
                        echo "</tr>";
                        }
                    }
            ?>
        </table>
    </div>
</body>
</html>