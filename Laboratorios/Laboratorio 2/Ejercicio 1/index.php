<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title> Ejercicio 1 </title>
    <link rel="stylesheet" type="text/css" href="Estilo/estilo.css">
</head>

<body>
    <div class="titulo">
        <h1> <u> Tabla de 10 x 10 con números del 1 al 100 </u> </h1>
    </div>
    <div class="tabla" style="margin: 0 auto">
        <table>
            <?php
                // Inicializamos el contador en 1 para los números de la tabla.
                $cont = 1;

                // Inicializamos las filas en 1.
                $fils = 1;

                // Primer ciclo para crear las filas.
                for($fils; $fils <= 10; $fils++){
                    // Se imprime el inicio de la fila.
                    echo "<tr>";

                    // Inicializamos las columnas en 1.
                    $cols = 1;
                    // Segundo ciclo para rellenar las columnas.
                    for($cols; $cols <=10; $cols++){
                        // Se imprime la columna y el número correspondiente.
                        echo "<td>", $cont, "</td>";

                        // Incrementamos el contador.
                        $cont = $cont + 1;
                    }

                    // Se imprime el cierre de la fila.
                    echo "</tr>";
                }
            ?>
        </table>
    </div>
</body>
</html>