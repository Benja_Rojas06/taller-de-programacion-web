<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title> Ejercicio 2 </title>
    <link rel="stylesheet" type="text/css" href="Estilo/estilo.css">
</head>

<body>
    <div class="titulo">
        <h1> <u> Tabla de N x N con función "define" </u> </h1>
    </div>

    <div class="tabla" style="margin: 0 auto">
        <table>
            <?php
                define("TAM", 10);
                    // Inicializamos el contador para los números de la tabla y las filas en 1.
                    $cont = 1;
                    $fils = 1;

                    // Primer ciclo para crear las filas.
                    for($fils; $fils <= TAM; $fils++){
                        // Se imprime el inicio de la fila.
                        // Cálculo de si la fila es impar; colorea en gris.
                        if($fils %2 == 1){
                            echo "<tr style='background-color:#808080'>";
                        }
                        // Sino es impar, colorea en blanco.
                        else{
                            echo "<tr style='background-color:#ffffff'>";
                        }

                        // Inicializamos las columnas en 1.
                        $cols = 1;
                        // Segundo ciclo para rellenar las columnas.
                        for($cols; $cols <=TAM; $cols++){
                            // Se imprime la columna y el número correspondiente.
                            echo "<td>", $cont, "</td>";

                            // Incrementamos el contador.
                            $cont = $cont + 1;
                        }

                        // Se imprime el cierre de la fila.
                        echo "</tr>";
                    }
            ?>
        </table>
    </div>
</body>
</html>