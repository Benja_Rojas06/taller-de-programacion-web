<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title> Ejercicio 4 </title>
    <link rel="stylesheet" type="text/css" href="Estilo/estilo.css">
</head>

<body>
    <div class="titulo">
        <h1> <u> Tabla con Fotos </u> </h1>
    </div>

    <table border= "1" style="margin: 0 auto">
        <tr>
            <?php
                // Se inicializa el contador en 0.
                $cont = 0;
                // Se indica la ubicación de las fotos (archivos JPEG).
                $ubicacion = "Fotos/";
                // Se indica que la ubicación se debe abrir.
                $archivo = opendir($ubicacion);

                // Ciclo para recorrer el archivo.
                while($imagen = readdir($archivo)){
                    // Se corrobora que no existan archivos vacíos.
                    if($imagen != "." && $imagen != ".."){
            ?>
                    <td>
                        <!-- Se insertan las imágenes dentro de la tabla. -->
                        <img src="<?php echo $ubicacion.$imagen ?>" width = "180px" height = "auto">
                    </td>
                    <?php
                        // Se aumenta el contador.
                        $cont++;

                        // Restricción para que la tabla tenga 4 columnas.
                        if($cont %4 == 0){
                            echo('</tr>');
                        }
                    }
                }
                // Se cierra el archivo.
                closedir($archivo);
                    ?>
        </tr>
    </table>

</body>

</html>