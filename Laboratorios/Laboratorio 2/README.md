# Propuesta Solución Laboratorio 1 Unidad 2

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Ayudante: Mauricio González.
- Módulo: Taller de Programación Web.

## Construcción del Programa:

- Sistema operativo: Microsoft Windows 10.
- Visual Studio Code: Editor de texto utilizado para escribir el código del programa.
- HTML y PHP: Lenguajes utilizados para resolver la problemática.
- Servidor web: Apache2.

## Ejercicios:

### Ejercicio 1:

- Código no es dinámico, por cual al inicializarse se genera la tabla de 10 x 10 de manera automática.
- Para cambiar el tamaño de las filas y/o columnas, se deben modificar los ciclos en los que se generan cada uno.

### Ejercicio 2:

- Código no es dinámico, por cual al inicializarse se genera la tabla de 10 x 10 de manera automática con la coloración indicada.
- Para cambiar la dimensión de la tabla (se genera solo a partir de la cantidad de filas), se debe cambiar la función "define(TAM, n)"; donde "n" es la dimensión a partir de la cual queremos generar nuestra tabla cuadrada.
- Para cambiar la coloración de las filas, solo basta con cambiar "background-color" dentro del ciclo que genera las filas.

### Ejercicio 3:

- Existen dos directorios dentro de la carpeta del ejercio, estos corresponden a GET y POST, donde en cada uno de estos se realiza el método que lleva su nombre para la generación de la tabla. Una vez nos encontramos dentro podremos observar que se nos pide ingresar el número de la cantidad de filas y el color de las filas pares e impares; por último debemos presionar el botón "Crear Tabla".

### Ejercicio 4:

- Al iniciar la página se genera automáticamente la tabla con la imágenes que se encuentran en el archivo "Fotos", a este directorio se pueden agregar más imágenes en formato "JPEG" para que la tabla muestre más fotos. Cabe mencionar que existe una restricción dentro del ciclo que crea la tabla para que esta solamente tenga 4 columnas, tal y como se solicita en el desarrollo del ejercicio.
