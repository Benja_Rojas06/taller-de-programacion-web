# Propuesta Solución Guía 1

- Creado por: Benjamín Rojas Rojas.
- Docente: Alejandro Valdés Jiménez.
- Módulo: Taller de Programación Web

## Construcción del Programa:

- Sistema operativo: Microsoft Windows 10.
- Visual Studio Code: Editor de texto utilizado para escribir el código del programa.
- Python 3.7.0: Lenguaje utilizado para resolver la problemática.

## Puntos a considerar:

- Se incluye el nombre completo de los aa, sus códigos de 3 y 1 letra, además de su descripción acompañada de una imagen referencial.
- Se escribe un código tabulado.
- La página de inicio y cada una de las páginas individuales (págs de aa y creador) contiene un navegador superior que
proporcione acceso a todos los lugares del sitio.
- Cada una de las páginas individuales contiene una imagen, una tabla y una lista de elementos.
- Se utiliza un único archivo de estilos CSS.

## Realizar revisión de History -> Browse Directory:

- Favor revisar el historial de los commits realizados. Accidentalmente borré los archivos que se encontraban en mi repositorio y, por ende, los volví a subir. Con esto han quedado "fuera de plazo" de envío el Laboratorio 1 y Guía 1. Sin embargo, en el historial se puede apreciar que los últimos cambios realizados sí entraban en el tiempo estipulado para el desarrollo de los mismos.