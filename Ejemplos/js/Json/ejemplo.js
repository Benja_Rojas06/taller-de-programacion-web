function cargar() {
    var xhttp = new XMLHttpRequest();
    
    // define una función que se llamará cuando cambie la propiedad readyState.
    xhttp.onreadystatechange = function() {
        // readyState 4: request finished and response is ready
        // status 200: "OK"
        if (this.readyState == 4 && this.status == 200) {
            // convierte a JSON los datos.
            let object = JSON.parse(this.responseText);
            
            let html_content = "";
            
            // muestra en la consola del navegador.
            console.log(object);
            console.log(object.name);
            console.log(object.age);
            
            // concatena contenido.
            html_content += "nombre: " + object.name + "<br>";
            html_content += "edad: " + object.age  + "<br>";
            html_content += "mascotas: <p></p>";
            html_content += "<ul>";
            
            object.pets.forEach(function (item) {
                console.log(item);
                html_content += "<li>" + item.name  + " (" + item.animal + ")</li>";
            });
            html_content += "</ul>";
            
            
            // copia el contenido.
            document.getElementById("contenido").innerHTML = html_content;
        }
    };
    
    // solicita archivo asíncronamente (true)
    xhttp.open("GET", "archivo.json", true);
    xhttp.send();
}