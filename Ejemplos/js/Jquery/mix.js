// se aplica cuando la página completa ha cargado.
$(document).ready(function() {
    /*
    $(this).hide() - hides the current element.
    $("p").hide() - hides all <p> elements.
    $("#test").hide() - hides the element with id="test".
    $(".test").hide() - hides all elements with class="test".
    */
            
    // evento click para todas las etiuqteas <p>
    $("p").click(function() {
        // oculta este mismo elemento.
        $(this).hide();
    });
            
    // evento click sobre botón con id="btn1"
    $("#btn1").click(function() {
        // oculta elemento con id="p1"
        $("#p1").hide();
    });
            
    // evento click sobre botón id="btn2"
    $("#btn2").click(function() {
        // oculta elementos con class="ocultar"
        $(".ocultar").hide();
    });
    
    // múltiples eventos sobre un elemento.
    $("h1").on({
        mouseenter: function(){
            $(this).css("background-color", "lightgray");
        },
        mouseleave: function(){
            $(this).css("background-color", "lightblue");
        },
        click: function(){
            $(this).css("background-color", "yellow");
        }
    });
});