// función que suma dos números.
function sumar(a, b) {
    // define una variable y realiza la suma.
    let suma = a + b;
    
    // imprime en la consola.
    console.log(suma);
    
    // muestra respuesta en popup.
    alert(suma);
}

// función imprime números.
function ciclo() {
    // solicita un número.
    let final = prompt("Introduce un número entero");
    
    // ciclo.
    for (i=1; i<=final; i++) {
        // obtiene el resto de una división en 2.
        if ((i%2) == 0) {
            // imprime en la consola.
            console.log("Par", i);
        } else {
            console.log("Impar", i);
        }
    }
}

// función para manipular objetos.
function imprime_objetos () {
    // lista (arreglo) para objetos.
    let lista = [];
    
    // crea objeto.
    let persona1 = new Object();
    persona1.nombre = "Alejandro";
    persona1.edad = 45;
    
    // lo agrega a la lista.
    lista.push(persona1);
    
    // crea otro objeto.
    let persona2 = {"nombre": "Matías", "edad": 14};
    // lo agrega a la lista.
    lista.push(persona2);
    
    // imprime objetos.
    lista.forEach(function (item) {
        // imprime en la consola.
        console.log("nombre", item.nombre, "edad", item.edad);
    });
}