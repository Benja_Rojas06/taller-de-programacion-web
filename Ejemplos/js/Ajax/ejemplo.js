function cargar() {
    var xhttp = new XMLHttpRequest();
    
    // define una función que se llamará cuando cambie la propiedad readyState.
    xhttp.onreadystatechange = function() {
        // readyState 4: request finished and response is ready
        // status 200: "OK"
        if (this.readyState == 4 && this.status == 200) {
            // muestra en la consola del navegador.
            console.log(this.responseText);
            // copia el contenido.
            document.getElementById("contenido").innerHTML = this.responseText;
        }
    };
    
    // solicita archivo asíncronamente (true)
    xhttp.open("GET", "archivo.txt", true);
    xhttp.send();
}
