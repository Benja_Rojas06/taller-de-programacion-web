from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import UsuarioCreationForm, UsuarioChangeForm
from .models import Usuario, Tipo, Ingrediente, Producto, Encargo, Detalle

class UsuarioAdmin(UserAdmin):
    add_form = UsuarioCreationForm
    form = UsuarioChangeForm
    model = Usuario
    list_display = ['email', 'username', 'first_name', 'last_name', 'numero_telefono']
    list_filter = ['first_name']

class TipoAdmin(admin.ModelAdmin):
    list_display = ['nombre']
    list_filter = ['nombre']

class IngredienteAdmin(admin.ModelAdmin):
    list_display = [ 'nombre']

class ProductoAdmin(admin.ModelAdmin):
    list_display = ['tipo', 'nombre', 'precio_actual', 'foto']
    list_filter = ['precio_actual']

class EncargoAdmin(admin.ModelAdmin):
    list_display = ['comprador'] 
    list_filter = ['comprador']

class DetalleAdmin(admin.ModelAdmin):
    list_display = ['encargo','precio_venta_antiguo', 'cantidad', 'echa_encargo'] 
    list_filter = ['encargo']



admin.site.register(Tipo, TipoAdmin)
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Ingrediente, IngredienteAdmin)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Encargo, EncargoAdmin)
admin.site.register(Detalle, DetalleAdmin)

