from datetime import datetime
import email
from email.message import EmailMessage

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.template import RequestContext, loader
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.contrib import messages
from django.db.models import Count, Sum
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
import json
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Group
from requests import session
from app.carrito import Carrito
from .models import Usuario, Tipo, Ingrediente, Producto, Encargo, Detalle
from .forms import ProductoForm, IngredienteForm, RegisterForm, TipoProdForm, User

# Decorador para verificar grupo.
def es_vendedor(user):
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo Vendedor?
        return user.groups.filter(name='Vendedor').exists()
    else:
        return False
    
# Vista Productos
@user_passes_test(es_vendedor)
def index(request):
    template = loader.get_template('vendedor/index.html')

    productos = Producto.objects.all()

    context = {
        'productos': productos,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))


# Vista Productos
@user_passes_test(es_vendedor)
def index(request):
    template = loader.get_template('vendedor/productos/index.html')

    productos = Producto.objects.all()

    context = {
        'productos': productos,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))

# Vista Ingredientes
@user_passes_test(es_vendedor)
def index_ingredientes(request):
    template = loader.get_template('vendedor/ingredientes/index.html')

    ingredientes = Ingrediente.objects.all()

    context = {
        'ingredientes': ingredientes,
        'settings': settings,
    }
    print(context)
    return HttpResponse(template.render(context, request))

# Vista Tipo Producto
@user_passes_test(es_vendedor)
def index_tipoprod(request):
    template = loader.get_template('vendedor/tipo_producto/index.html')

    tipo_producto = Tipo.objects.all()

    context = {
        'tipo_producto': tipo_producto,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))

"""
Productos
"""

# Agrega
@user_passes_test(es_vendedor)
def add(request):
    if (request.method == 'GET'):
        form = ProductoForm()
            
        # mostrar formulario.
        template = loader.get_template('vendedor/productos/add.html')
    
        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }
        
        return HttpResponse(template.render(context, request))
    
    elif (request.method == 'POST'):
        # guardar datos.
        form = ProductoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index')
    
# Elimina
@user_passes_test(es_vendedor)
def delete(request, id):
    
    producto = Producto.objects.get(id=id)
    
    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('vendedor/productos/delete.html')

        context = {
            'settings': settings,
            'producto': producto,
        }
    
        return HttpResponse(template.render(context, request))
        
    elif (request.method == 'POST'):
        # elimina la publicación.
        producto.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index')

# Actualiza
@user_passes_test(es_vendedor)
def update(request, id):
    producto = Producto.objects.get(id=id)
    
    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = ProductoForm(instance=producto)
        #form.fields['investigador'].queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        template = loader.get_template('vendedor/productos/add.html')

        context = {
            'producto': producto,
            'settings': settings,
            'form': form,
            'isadd': False,
        }
        
        return HttpResponse(template.render(context, request))
        
    elif (request.method == 'POST'):
        # actualiza datos.
        form = ProductoForm(request.POST, instance=producto)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index')

"""
Ingredientes
"""

# Agrega
@user_passes_test(es_vendedor)
def add_ingredientes(request):
    if (request.method == 'GET'):
        form = IngredienteForm()
        # muesta usarios de esos grupos.
        #form.fields['Vendedor'].queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        
        # mostrar formulario.
        template = loader.get_template('vendedor/ingredientes/add.html')
    
        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }
        
        return HttpResponse(template.render(context, request))
    
    elif (request.method == 'POST'):
        # guardar datos.
        form = IngredienteForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index_ingredientes')
        
# Elimina
@user_passes_test(es_vendedor)
def delete_ingredientes(request, id):
    
    ingrediente = Ingrediente.objects.get(id=id)
    
    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('vendedor/ingredientes/delete.html')

        context = {
            'settings': settings,
            'ingrediente': ingrediente,
        }
    
        return HttpResponse(template.render(context, request))
        
    elif (request.method == 'POST'):
        # elimina la publicación.
        ingrediente.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_ingredientes')
    
# Actualiza
@user_passes_test(es_vendedor)
def update_ingredientes(request, id):
    ingrediente = Ingrediente.objects.get(id=id)
    
    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = IngredienteForm(instance=ingrediente)
        #form.fields['investigador'].queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        template = loader.get_template('vendedor/ingredientes/add.html')

        context = {
            'ingrediente': ingrediente,
            'settings': settings,
            'form': form,
            'isadd': False,
        }
        
        return HttpResponse(template.render(context, request))
        
    elif (request.method == 'POST'):
        # actualiza datos.
        form = IngredienteForm(request.POST, instance=ingrediente)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_ingredientes')
    
"""
Tipo Producto
"""

# Agrega
@user_passes_test(es_vendedor)
def add_tipoprod(request):
    if (request.method == 'GET'):
        form = TipoProdForm()
        # muesta usarios de esos grupos.
        #form.fields['Vendedor'].queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        
        # mostrar formulario.
        template = loader.get_template('vendedor/tipo_producto/add.html')
    
        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }
        
        return HttpResponse(template.render(context, request))
    
    elif (request.method == 'POST'):
        # guardar datos.
        form = TipoProdForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('index_tipoprod')
    
# Elimina
@user_passes_test(es_vendedor)
def delete_tipoprod(request, id):
    
    tipo_producto = Tipo.objects.get(id=id)
    
    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('vendedor/tipo_producto/delete.html')

        context = {
            'settings': settings,
            'tipo_producto': tipo_producto,
        }
    
        return HttpResponse(template.render(context, request))
        
    elif (request.method == 'POST'):
        # elimina la tipo producto.
        tipo_producto.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('index_tipoprod')
    
# Actualiza
@user_passes_test(es_vendedor)
def update_tipoprod(request, id):
    tipo_producto = Tipo.objects.get(id=id)
    
    if (request.method == 'GET'):
        # muestra el formulario con datos.
        form = TipoProdForm(instance=tipo_producto)
        #form.fields['investigador'].queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        template = loader.get_template('vendedor/ingredientes/add.html')

        context = {
            'tipo_producto': tipo_producto,
            'settings': settings,
            'form': form,
            'isadd': False,
        }
        
        return HttpResponse(template.render(context, request))
        
    elif (request.method == 'POST'):
        # actualiza datos.
        form = TipoProdForm(request.POST, instance=tipo_producto)
        if form.is_valid():
            form.save()
            messages.success(request, "Actualización realizada.")

        return redirect('index_tipoprod')


# Gráfico 1
def grafico(request):
    # variable de session num_visitas.
    num_visitas = request.session.get('num_visitas', 0)
    request.session['num_visitas'] = num_visitas + 1

    # default template.
    template = loader.get_template('vendedor/grafico_ingredientes/dashboard.html')

    productos = Producto.objects.all()

    # agrupa número de productos por ingredientes.
    result = (productos.values('ingredientes').annotate(dcount=Count('ingredientes'))).order_by('ingredientes')
     # copia a un diccionario el rsultado.
    dataarticulos = []
    print(result)
    #count = 0
    for item in result:
        print(item)
        ingredientes = Ingrediente.objects.filter(id=item['ingredientes']).values()[0]
        dataarticulos.append({'label': ingredientes["nombre"], 'y': item["dcount"]})
  
    context = {
        'settings': settings,
        'dataarticulos': json.dumps(dataarticulos),
        'num_visitas': num_visitas,
    }
    return HttpResponse(template.render(context, request))

# Gráfico 2
def grafico2(request):
     # variable de session num_visitas.
    num_visitas = request.session.get('num_visitas', 0)
    request.session['num_visitas'] = num_visitas + 1

    # default template.
    template = loader.get_template('vendedor/grafico_productos/dashboard.html')
     # gráfico ejemplares.
    detalles = Detalle.objects.all()

    print(detalles)
    result2 = (detalles.values('producto').annotate(dcount=Count('producto')))
    dataejemplares = []
    for item in result2:
        obj = Producto.objects.filter(id=item['producto']).values()[0]
        dataejemplares.append({'label': obj["nombre"], 'y': item['dcount']})

    context = {
        'settings': settings,
        'dataejemplares': json.dumps(dataejemplares),
        'num_visitas': num_visitas,
    }

    #a_csv(dataarticulos, dataejemplares)
    return HttpResponse(template.render(context, request))

# Gráfico 3
def grafico3(request):
    # variable de session num_visitas.
    num_visitas = request.session.get('num_visitas', 0)
    request.session['num_visitas'] = num_visitas + 1

    # default template.
    template = loader.get_template('vendedor/grafico_compras/dashboard.html')

    detalles = Encargo.objects.all()

    # agrupa número de productos por ingredientes.
    result = (detalles.values('comprador').annotate(dcount=Count('comprador'))).order_by('comprador')
    # copia a un diccionario el rsultado.
    dataarticulos = []

    #count = 0
    for item in result:
        usuarios = Usuario.objects.filter(id=item['comprador']).values()[0]
        dataarticulos.append({'label': usuarios["first_name"], 'y': item["dcount"]})
    
    context = {
        'settings': settings,
        'dataarticulos': json.dumps(dataarticulos),
        'num_visitas': num_visitas,
    }
    return HttpResponse(template.render(context, request))


# Decorador para verificar grupo.
def es_cliente(user):
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo Cliente?
        return user.groups.filter(name='Cliente').exists()
    else:
        return False
    
@user_passes_test(es_cliente)
def index_cliente(request):
    template = loader.get_template('cliente/index.html')

    productos = Producto.objects.all()

    context = {
        'productos': productos,
        'settings': settings,
    }
    return HttpResponse(template.render(context, request))


def Registro(request):
    # Cargamos los fields desde forms.py
    data = {
        'form': RegisterForm()
    }
    # Metodo de accion por POST
    if request.method == 'POST':
        # Asignamos el formulario con los datos ingresados
        formulario  = RegisterForm(data=request.POST)
        # Fomulario debe estar lleno
        if formulario.is_valid():
            # Guardamos al usuario con los datos del formulario
            user = formulario.save()
            # Obtenemos el grupo filtrando **en mi caso es cliente**
            my_group = Group.objects.get(name='Cliente')
            # Agregamos el grupo cliente al nuevo usuario
            my_group.user_set.add(user)
            # Lo logeamos 
            # No hace falta autenticacion pq nosotros le dimos el grupo
            login(request,user) 
            # Se envía correo de confirmación de registro
            enviar_correo_confirmacion(request, user)
            # Error qlo no se pq pero lo vi en overflow owo
            if user is not None:
                # Mensaje de bienvenida
                messages.success(request,"Bienvenido")
                # Mostramos mensaje por terminal
                print("El {} ha sido agregado al grupo {}".format(user,my_group))
                # Lo mando a la vista de clientes
                return redirect(to = "index_cliente")
            
        data["form"] = formulario
    # cargamos plantilla de registro
    return render(request,"registration/registro.html", data)


def agregar_productos_carrito(request, producto_id):
    carrito = Carrito(request)
    producto = Producto.objects.get(id=producto_id)
    
    productos_carrito = {"nombre":[], "precio": 0, "cantidad": 0}
    session = request.session.get("data", productos_carrito)
    
    session["nombre"].append(producto.nombre)
    session["precio"] += int(producto.precio_actual)
    session["cantidad"] += 1
    request.session["data"]=session
    messages.success(request, "Producto agregado de manera correcta") 

    print(" {} ha sido agregado al carrito".format(producto))
    print("Usuario comprando {}".format(User.objects.get(username = request.user.username)))
    
    carrito.agregar(producto)
    
    Encargo.objects.create(comprador= User.objects.get(username = request.user.username))
    
    return redirect('index_cliente')

def carrito_compras(request):
    print("Resúmen Carrito \n Productos comprados: {} \n Cantidad Productos: {} \n Precio Total: {}".format(request.session['data']['nombre'],
                                                                                                            request.session['data']['cantidad'],
                                                                                                            request.session['data']['precio']             
                                                                                                            ))
    
    #cliente = Encargo.objects.get(comprador = request.user.username)
    
    #Detalle.objects.create(producto=request.session['data']['nombre'])
    
    print("Botón guardar")
    enviar_correo_carrito(request)
    return redirect('index_cliente')

def eliminar_producto_carrito(request, producto_id):
    carrito = Carrito(request)
    producto = Producto.objects.get(id=producto_id)
    carrito.eliminar(producto)
    return redirect('index_cliente')

def restar_producto_carrito(request, producto_id):
    carrito = Carrito(request)
    producto = Producto.objects.get(id=producto_id)
    carrito.restar(producto)
    return redirect('index_cliente')

def limpiar_producto_carrito(request):
    carrito = Carrito(request)
    carrito.limpiar()
    try: 
        del request.session['data']
    except KeyError:
        pass
    
    return redirect('index_cliente')

# Vista Productos
@user_passes_test(es_vendedor)
def historial_vendedor_detalles(request):
    template = loader.get_template('vendedor/Detalles/index.html')

    encargos = Encargo.objects.all()
    detalles = Detalle.objects.all()

    context = {
        'detalles': detalles,
        'settings': settings,
        'encargos': encargos,
    }
    print(detalles)
    return HttpResponse(template.render(context, request))

# Vista Productos
@user_passes_test(es_cliente)
def historial_cliente(request):
    template = loader.get_template('cliente/Pedidos/pedidos.html')

    encargos = Encargo.objects.all()
    detalles = Detalle.objects.all()

    context = {
        'detalles': detalles,
        'settings': settings,
        'encargos': encargos,
    }
    return HttpResponse(template.render(context, request))


# Vista Productos
@user_passes_test(es_vendedor)
def historial_cliente_vendedor(request):
    template = loader.get_template('vendedor/Clientes/index.html')
    usuarios = User.objects.all()
    print(usuarios)
    context = {
        'settings': settings,
        'usuarios': usuarios,
    }

    return HttpResponse(template.render(context,request))


def crear_correo(mail, subject, template_path, context):
    
    # Se define contenidp del correo a través del template.
    template = get_template(template_path)
    content = template.render(context)
    
    # Se genera el correo.
    email = EmailMultiAlternatives(
        subject=subject,
        body='',
        from_email= settings.EMAIL_HOST_USER,
        to=[
            mail
        ],
        # Copia a administrador
        cc=[
            settings.EMAIL_HOST_USER
            ]
    )
    
    # Se agrega contenido al correo.
    email.attach_alternative(content, 'text/html')
    return email


def confirmacion_registro(user):
    bienvenida = crear_correo(
        user.email,
        'Confirmación de Registro',
        'cliente/Correos/conf_registro.html',
        {
            'username': user
        }
    )
    
    bienvenida.send(fail_silently=False)


def enviar_correo_confirmacion(request, user):
    confirmacion_registro(user)
    
    return render(request, 'cliente/index.html')


def correo_contacto(request):
    if request.method == "POST":
        contacto = crear_correo(
            settings.EMAIL_HOST_USER,
            'Este cliente necesita ayuda!!!',
            'cliente/Correos/contacto/envio_contacto.html',
            {
                'username': request.POST["nombre"],
                'numero': request.POST["numero"],
                'mail': request.POST["mail"],
                'mensaje': request.POST["mensaje"]
            }
        )
        contacto.send(fail_silently=False)
        
        print("Correo enviado con éxito")
    
    
def enviar_correo_contacto(request):
    correo_contacto(request)
    
    return render(request, 'cliente/Correos/contacto/contacto.html')


def correo_carrito(request): 
    carrito = crear_correo(
        request.user.email,
        'Solicitud de Compra',
        'cliente/Correos/carrito.html',
        {
            'username': request.user,
            'productos': request.session['data']['nombre'],
            'cantidad': request.session['data']['cantidad'],
            'precio': request.session['data']['precio'],
            
            'correo': request.user.email,
            'numero': request.user.numero_telefono
        }
    )
    carrito.send(fail_silently=False)


def enviar_correo_carrito(request):
    correo_carrito(request)
    
    return render(request, 'cliente/index.html')

