from django.contrib.auth.models import AbstractUser
from django.db import models

class Usuario(AbstractUser):
    email = models.EmailField('Correo ', unique = True)
    numero_telefono = models.IntegerField("Celular ", null = True, unique = True)
    username = models.CharField("Usuario", max_length=100, null = False)
    first_name = models.CharField("Nombre", max_length=100, null = False)
    last_name = models.CharField("Apellido", max_length=100, null = False)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', 'numero_telefono']
    

    def __str__(self):
        return self.first_name + ' ' + self.last_name   

    class Meta:
    
        verbose_name_plural = 'Usuarios Registrados'


class Tipo(models.Model):
    nombre = models.CharField(max_length=15, unique=True)
    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Clasificación de Productos"

class Ingrediente(models.Model):
    nombre = models.CharField(max_length=20, unique=True)
    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name_plural = "Stock de Ingredientes"

class Producto(models.Model):
    tipo = models.ForeignKey(to=Tipo, on_delete=models.CASCADE, null=True, blank=True)
    nombre = models.CharField(max_length=30, unique=True)
    precio_actual = models.PositiveSmallIntegerField(null=True)
    ingredientes = models.ManyToManyField(Ingrediente, verbose_name = 'Escoga ingredientes')
    foto = models.ImageField(upload_to="imagenes/", null=True)
    descripcion = models.CharField(max_length=500, default='Ingrese descripcion')

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Stock de Productos"

class Encargo(models.Model):

    #estado (retirado o no)
    comprador = models.ForeignKey(Usuario, on_delete = models.CASCADE, null=True)
    
    

    def __str__(self):
        return str(self.comprador)

    class Meta:

        verbose_name_plural = 'Encargas Solicitidas'
        

class Detalle(models.Model):

    #class Meta:
        #unique_together = (('encargo', 'producto'),)

    encargo = models.ForeignKey(Encargo, verbose_name = 'Encargado por', on_delete= models.CASCADE, null=True)
    producto = models.ManyToManyField(Producto, verbose_name = 'Producto')
    precio_venta_antiguo = models.PositiveSmallIntegerField(null=True, verbose_name = 'Precio') 
    echa_encargo = models.DateField(null=True, verbose_name = 'Fecha de Encargo')
    cantidad = models.PositiveSmallIntegerField(null=True)
    
    @property
    def precio_final(self):
        if(self.cantidad == 1):
            precio_final = self.precio_venta_antiguo
            return precio_final
        else:
            precio_final = self.precio_venta_antiguo * self.cantidad
            return precio_final