
from django.urls import path
from . import views

from django.conf import settings
from django.contrib.staticfiles.urls import static

urlpatterns = [
    # Productos
    path('productos/', views.index, name='index'),
    path('productos/add/', views.add, name='add'),
    path('productos/delete/<int:id>', views.delete, name='delete'),
    path('productos/update/<int:id>', views.update, name='update'),

    # Ingredientes
    path('ingredientes/', views.index_ingredientes, name='index_ingredientes'),
    path('ingredientes/add/', views.add_ingredientes, name='add_ingredientes'),
    path('ingredientes/delete/<int:id>', views.delete_ingredientes, name='delete_ingredientes'),
    path('ingredientes/update/<int:id>', views.update_ingredientes, name='update_ingredientes'),
    
    # Tipo Producto
    path('tipo_producto/', views.index_tipoprod, name='index_tipoprod'),
    path('tipo_producto/add/', views.add_tipoprod, name='add_tipoprod'),
    path('tipo_producto/delete/<int:id>', views.delete_tipoprod, name='delete_tipoprod'),
    path('tipo_producto/update/<int:id>', views.update_tipoprod, name='update_tipoprod'),
    
    # Gráficos
    path('grafico_ingredientes/', views.grafico, name='grafico'),
    path('grafico_productos/', views.grafico2, name = 'grafico2'),
    path('grafico_compras/', views.grafico3, name = 'grafico3'),

    # Historiales
    path('cliente/', views.index_cliente, name = "index_cliente"),
    path('Clientes/', views.historial_cliente_vendedor, name = "index_cliente_vendedor"),                 
    
    path('registration/', views.Registro, name = 'registro'),
    path('agregar/<int:producto_id>/', views.agregar_productos_carrito, name="Add"),
    path('eliminar/<int:producto_id>/', views.limpiar_producto_carrito, name="Del"),
    path('restar/<int:producto_id>/', views.restar_producto_carrito, name="Sub"),
    path('limpiar/', views.limpiar_producto_carrito, name="CLS"),

    path('Detalles/', views.historial_vendedor_detalles, name='historial_vendedor_detalles'),
    path('Pedidos/', views.historial_cliente, name = 'historial_cliente'),
    
    path('Correos/conf_registro/', views.enviar_correo_confirmacion, name = 'enviar_correo_confirmacion'),
    path('Correos/contacto/', views.enviar_correo_contacto, name = 'enviar_correo_contacto'),
    path('Correos/carrito/', views.enviar_correo_carrito, name = 'enviar_correo_carrito'),
    
    path('carrito_compras/', views.carrito_compras, name = 'guardar'),
]       

