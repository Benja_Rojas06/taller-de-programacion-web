from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Usuario, Tipo, Producto, Ingrediente, Encargo, Detalle
from django.contrib.auth import get_user_model


User = get_user_model()
class UsuarioCreationForm(UserCreationForm):
    class Meta:
        model = Usuario
        fields = ("username", "email", 'numero_telefono')

class UsuarioChangeForm(UserChangeForm):
    class Meta:
        model = Usuario
        fields = ("username", "email", 'numero_telefono')

# Producto
class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto
        #fields = ("tipo", "nombre", "precio_actual", "ingredientes", "descripcion")
        fields = ("tipo", "nombre", "precio_actual", "ingredientes", "foto", "descripcion")
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'tipo': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
            'precio_actual': forms.NumberInput(attrs={'class': 'form-control', 'required': True}),
            'ingredientes': forms.SelectMultiple(attrs={'class': 'form-control', 'required': True}),
            'descripcion': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
        }
        # foto: forms.ImageField()

# Ingredientes
class IngredienteForm(forms.ModelForm):    
    class Meta:
        model = Ingrediente
        fields = ("nombre",)
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
        }

# Tipo Producto
class TipoProdForm(forms.ModelForm):    
    class Meta:
        model = Tipo
        fields = ("nombre",)
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'required': True}),
        }
        
class RegisterForm(UserCreationForm):
    email = forms.EmailField(
        label= 'Correo Electrónico',
        required=True,
    )
    numero_telefono = forms.IntegerField(
        label = "Ingrese su Numero",
        required=True,
    )
    username = forms.CharField(
        label = 'Nombre de Usuario',
        required=True,
    )
    first_name = forms.CharField(
        label = "Ingrese su Nombre",
        required=True,
    
    )
    last_name = forms.CharField(
        label = "Ingrese su Apellido",
        required=True,
    )
    password1 = forms.CharField(
        label = "Contraseña",
        required=True,
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label = "Confirmar Contraseña",
        required=True,
        widget=forms.PasswordInput
    )
    class Meta:
        model = User
        fields = ['username','first_name', 'last_name', 'email', 'numero_telefono', 'password1', 'password2']



