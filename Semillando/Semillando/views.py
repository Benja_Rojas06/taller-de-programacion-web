from django.shortcuts import render
from django.template import loader
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
import json
from django.db.models import Count
from django.db.models.functions import TruncYear

from app.models import Producto


# Create your views here.

def home(request):
        
    # default template.
    template = loader.get_template('index.html')
    context = {
        'settings': settings,
        
    }
    
    # Está autenticado?
    if (request.user.is_authenticated):
        
        # Es superusuario?
        if (request.user.is_superuser):
            return HttpResponseRedirect('admin/')
        
        # pertene a cierto grupo?
        if (len(request.user.groups.all()) > 0):
            # revisar el grupo (el primero).
            grupo = request.user.groups.all()[0]
            
            # cambia el template.
            if (grupo.name == "Cliente"):
                template = loader.get_template('cliente/index.html')
                
                    
                productos = Producto.objects.all()

                context = {
                'productos': productos,
                'settings': settings,
                }
                return HttpResponse(template.render(context, request))

            if(grupo.name == "Vendedor"):

                template = loader.get_template('vendedor/index.html')
                productos = Producto.objects.all()

                context = {
                'productos': productos,
                'settings': settings,
                }

                return HttpResponse(template.render(context, request))
            
        else:
           pass
    else:
        pass
    print(context)         
    return HttpResponse(template.render(context, request))