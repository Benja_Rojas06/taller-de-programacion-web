--
-- PostgreSQL database dump
--

-- Dumped from database version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: app_detalle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_detalle (
    id bigint NOT NULL,
    precio_venta_antiguo smallint,
    encargo_id bigint,
    cantidad smallint,
    producto_id bigint,
    fecha_encargo date,
    CONSTRAINT app_detalle_cantidad_check CHECK ((cantidad >= 0)),
    CONSTRAINT app_detalle_precio_venta_antiguo_check CHECK ((precio_venta_antiguo >= 0))
);


ALTER TABLE public.app_detalle OWNER TO postgres;

--
-- Name: app_detalle_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_detalle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_detalle_id_seq OWNER TO postgres;

--
-- Name: app_detalle_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_detalle_id_seq OWNED BY public.app_detalle.id;


--
-- Name: app_encargo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_encargo (
    id bigint NOT NULL,
    comprador_id bigint
);


ALTER TABLE public.app_encargo OWNER TO postgres;

--
-- Name: app_encargo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_encargo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_encargo_id_seq OWNER TO postgres;

--
-- Name: app_encargo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_encargo_id_seq OWNED BY public.app_encargo.id;


--
-- Name: app_encargo_producto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_encargo_producto (
    id bigint NOT NULL,
    encargo_id bigint NOT NULL,
    producto_id bigint NOT NULL
);


ALTER TABLE public.app_encargo_producto OWNER TO postgres;

--
-- Name: app_encargo_producto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_encargo_producto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_encargo_producto_id_seq OWNER TO postgres;

--
-- Name: app_encargo_producto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_encargo_producto_id_seq OWNED BY public.app_encargo_producto.id;


--
-- Name: app_ingrediente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_ingrediente (
    id bigint NOT NULL,
    nombre character varying(20) NOT NULL
);


ALTER TABLE public.app_ingrediente OWNER TO postgres;

--
-- Name: app_ingrediente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_ingrediente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_ingrediente_id_seq OWNER TO postgres;

--
-- Name: app_ingrediente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_ingrediente_id_seq OWNED BY public.app_ingrediente.id;


--
-- Name: app_producto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_producto (
    id bigint NOT NULL,
    nombre character varying(30) NOT NULL,
    precio_actual smallint,
    foto character varying(100),
    descripcion character varying(500) NOT NULL,
    tipo_id bigint,
    CONSTRAINT app_producto_precio_actual_check CHECK ((precio_actual >= 0))
);


ALTER TABLE public.app_producto OWNER TO postgres;

--
-- Name: app_producto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_producto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_producto_id_seq OWNER TO postgres;

--
-- Name: app_producto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_producto_id_seq OWNED BY public.app_producto.id;


--
-- Name: app_producto_ingredientes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_producto_ingredientes (
    id bigint NOT NULL,
    producto_id bigint NOT NULL,
    ingrediente_id bigint NOT NULL
);


ALTER TABLE public.app_producto_ingredientes OWNER TO postgres;

--
-- Name: app_producto_ingredientes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_producto_ingredientes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_producto_ingredientes_id_seq OWNER TO postgres;

--
-- Name: app_producto_ingredientes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_producto_ingredientes_id_seq OWNED BY public.app_producto_ingredientes.id;


--
-- Name: app_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_tipo (
    id bigint NOT NULL,
    nombre character varying(15) NOT NULL
);


ALTER TABLE public.app_tipo OWNER TO postgres;

--
-- Name: app_tipo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_tipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_tipo_id_seq OWNER TO postgres;

--
-- Name: app_tipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_tipo_id_seq OWNED BY public.app_tipo.id;


--
-- Name: app_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_usuario (
    id bigint NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL,
    numero_telefono integer,
    username character varying(100) NOT NULL,
    first_name character varying(100) NOT NULL,
    last_name character varying(100) NOT NULL
);


ALTER TABLE public.app_usuario OWNER TO postgres;

--
-- Name: app_usuario_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_usuario_groups (
    id bigint NOT NULL,
    usuario_id bigint NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.app_usuario_groups OWNER TO postgres;

--
-- Name: app_usuario_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_usuario_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_usuario_groups_id_seq OWNER TO postgres;

--
-- Name: app_usuario_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_usuario_groups_id_seq OWNED BY public.app_usuario_groups.id;


--
-- Name: app_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_usuario_id_seq OWNER TO postgres;

--
-- Name: app_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_usuario_id_seq OWNED BY public.app_usuario.id;


--
-- Name: app_usuario_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_usuario_user_permissions (
    id bigint NOT NULL,
    usuario_id bigint NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.app_usuario_user_permissions OWNER TO postgres;

--
-- Name: app_usuario_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_usuario_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_usuario_user_permissions_id_seq OWNER TO postgres;

--
-- Name: app_usuario_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_usuario_user_permissions_id_seq OWNED BY public.app_usuario_user_permissions.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id bigint NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id bigint NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: app_detalle id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_detalle ALTER COLUMN id SET DEFAULT nextval('public.app_detalle_id_seq'::regclass);


--
-- Name: app_encargo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_encargo ALTER COLUMN id SET DEFAULT nextval('public.app_encargo_id_seq'::regclass);


--
-- Name: app_encargo_producto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_encargo_producto ALTER COLUMN id SET DEFAULT nextval('public.app_encargo_producto_id_seq'::regclass);


--
-- Name: app_ingrediente id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_ingrediente ALTER COLUMN id SET DEFAULT nextval('public.app_ingrediente_id_seq'::regclass);


--
-- Name: app_producto id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_producto ALTER COLUMN id SET DEFAULT nextval('public.app_producto_id_seq'::regclass);


--
-- Name: app_producto_ingredientes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_producto_ingredientes ALTER COLUMN id SET DEFAULT nextval('public.app_producto_ingredientes_id_seq'::regclass);


--
-- Name: app_tipo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_tipo ALTER COLUMN id SET DEFAULT nextval('public.app_tipo_id_seq'::regclass);


--
-- Name: app_usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario ALTER COLUMN id SET DEFAULT nextval('public.app_usuario_id_seq'::regclass);


--
-- Name: app_usuario_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_groups ALTER COLUMN id SET DEFAULT nextval('public.app_usuario_groups_id_seq'::regclass);


--
-- Name: app_usuario_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.app_usuario_user_permissions_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Data for Name: app_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_detalle (id, precio_venta_antiguo, encargo_id, cantidad, producto_id, fecha_encargo) FROM stdin;
6	4000	9	2	2	2022-07-05
5	2000	10	1	1	2022-07-05
\.


--
-- Data for Name: app_encargo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_encargo (id, comprador_id) FROM stdin;
9	4
10	1
\.


--
-- Data for Name: app_encargo_producto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_encargo_producto (id, encargo_id, producto_id) FROM stdin;
1	9	1
2	9	2
3	10	1
\.


--
-- Data for Name: app_ingrediente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_ingrediente (id, nombre) FROM stdin;
1	Avellana
2	Quinoa
3	Avena
4	Coco rallado
\.


--
-- Data for Name: app_producto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_producto (id, nombre, precio_actual, foto, descripcion, tipo_id) FROM stdin;
1	Granola la Cleo	4500	imagenes/Captura_de_pantalla_de_2022-06-29_12-52-20.png	Hola Mundo	2
2	Mantequilla de Merquèn	1000	imagenes/Captura_de_pantalla_de_2022-06-25_19-11-02.png	xd	1
\.


--
-- Data for Name: app_producto_ingredientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_producto_ingredientes (id, producto_id, ingrediente_id) FROM stdin;
1	1	1
2	1	3
3	2	4
\.


--
-- Data for Name: app_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_tipo (id, nombre) FROM stdin;
1	Mantequilla
2	Granola
3	Especial
\.


--
-- Data for Name: app_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_usuario (id, password, last_login, is_superuser, is_staff, is_active, date_joined, email, numero_telefono, username, first_name, last_name) FROM stdin;
4	pbkdf2_sha256$320000$RpqU1WrlhFxoUVnStEXeIs$sZ9+Iv1qC3McFMAXiWjv2a9T9axFMLxB8SMd03s85XI=	2022-07-05 13:29:24.747104-04	f	f	t	2022-07-05 13:29:24.542225-04	fransico@amigo.cl	133	pancho	Francisco	Amigo
1	pbkdf2_sha256$320000$eovx2X996vSfjMXNkNweqD$lO6eKeLy3lbxomdzhvfEx5isM8gbGDex8oxWlTsCrSY=	2022-07-05 16:42:58.606111-04	t	t	t	2022-07-05 13:14:24.602541-04	semillando@admin.cl	945159150	Admin	Sebastian	Tapia
3	pbkdf2_sha256$320000$IMGuIjAM07uHJ4CcPax7Rm$/Ejvlkd6BvPISJzBgvjiVQukBzeM/ENnCbJCUufMKTw=	2022-07-05 16:43:35.679444-04	f	f	t	2022-07-05 13:16:55-04	semillando@vendedor.cl	\N	Vendedor	Semillando	Vendedor
\.


--
-- Data for Name: app_usuario_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_usuario_groups (id, usuario_id, group_id) FROM stdin;
1	3	2
2	4	1
\.


--
-- Data for Name: app_usuario_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_usuario_user_permissions (id, usuario_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
1	Cliente
2	Vendedor
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add usuario	6	add_usuario
22	Can change usuario	6	change_usuario
23	Can delete usuario	6	delete_usuario
24	Can view usuario	6	view_usuario
25	Can add ingrediente	7	add_ingrediente
26	Can change ingrediente	7	change_ingrediente
27	Can delete ingrediente	7	delete_ingrediente
28	Can view ingrediente	7	view_ingrediente
29	Can add tipo	8	add_tipo
30	Can change tipo	8	change_tipo
31	Can delete tipo	8	delete_tipo
32	Can view tipo	8	view_tipo
33	Can add producto	9	add_producto
34	Can change producto	9	change_producto
35	Can delete producto	9	delete_producto
36	Can view producto	9	view_producto
37	Can add encargo	10	add_encargo
38	Can change encargo	10	change_encargo
39	Can delete encargo	10	delete_encargo
40	Can view encargo	10	view_encargo
41	Can add detalle	11	add_detalle
42	Can change detalle	11	change_detalle
43	Can delete detalle	11	delete_detalle
44	Can view detalle	11	view_detalle
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2022-07-05 13:15:24.385638-04	1	Cliente	1	[{"added": {}}]	3	1
2	2022-07-05 13:15:55.816372-04	2	 	1	[{"added": {}}]	6	1
3	2022-07-05 13:16:43.601756-04	2	 	3		6	1
4	2022-07-05 13:16:56.108182-04	3	 	1	[{"added": {}}]	6	1
5	2022-07-05 13:17:22.544992-04	2	Vendedor	1	[{"added": {}}]	3	1
6	2022-07-05 13:17:31.335489-04	3	Semillando Vendedor	2	[{"changed": {"fields": ["Nombre", "Apellido", "Correo ", "Groups"]}}]	6	1
7	2022-07-05 13:28:14.197317-04	1	Sebastian Tapia	1	[{"added": {}}]	10	1
8	2022-07-05 13:28:25.601979-04	1	Detalle object (1)	1	[{"added": {}}]	11	1
9	2022-07-05 13:37:10.779095-04	2	Francisco Amigo	1	[{"added": {}}]	10	1
10	2022-07-05 13:37:19.185327-04	3	Francisco Amigo	1	[{"added": {}}]	10	1
11	2022-07-05 14:58:41.070106-04	4	Francisco Amigo	1	[{"added": {}}]	10	1
12	2022-07-05 15:05:15.810723-04	5	Sebastian Tapia	1	[{"added": {}}]	10	1
13	2022-07-05 15:18:25.352939-04	5	Sebastian Tapia	3		10	1
14	2022-07-05 15:18:25.35697-04	4	Francisco Amigo	3		10	1
15	2022-07-05 15:18:25.359379-04	3	Francisco Amigo	3		10	1
16	2022-07-05 15:18:25.362449-04	2	Francisco Amigo	3		10	1
17	2022-07-05 15:18:25.366356-04	1	Sebastian Tapia	3		10	1
18	2022-07-05 15:18:33.397817-04	6	Sebastian Tapia	1	[{"added": {}}]	10	1
19	2022-07-05 15:18:46.447499-04	7	Francisco Amigo	1	[{"added": {}}]	10	1
20	2022-07-05 16:06:31.158744-04	2	Detalle object (2)	1	[{"added": {}}]	11	1
21	2022-07-05 16:08:16.795384-04	8	Francisco Amigo	1	[{"added": {}}]	10	1
22	2022-07-05 16:08:39.773436-04	3	Detalle object (3)	1	[{"added": {}}]	11	1
23	2022-07-05 16:08:54.054543-04	4	Detalle object (4)	1	[{"added": {}}]	11	1
24	2022-07-05 16:11:22.288782-04	8	Francisco Amigo	3		10	1
25	2022-07-05 16:11:22.297992-04	7	Francisco Amigo	3		10	1
26	2022-07-05 16:11:22.301144-04	6	Sebastian Tapia	3		10	1
27	2022-07-05 16:11:29.549521-04	9	Francisco Amigo	1	[{"added": {}}]	10	1
28	2022-07-05 16:12:05.484302-04	10	Sebastian Tapia	1	[{"added": {}}]	10	1
29	2022-07-05 16:13:15.741416-04	5	Detalle object (5)	1	[{"added": {}}]	11	1
30	2022-07-05 16:13:26.630882-04	6	Detalle object (6)	1	[{"added": {}}]	11	1
31	2022-07-05 16:43:25.487223-04	6	Detalle object (6)	2	[{"changed": {"fields": ["Fecha de Encargo"]}}]	11	1
32	2022-07-05 16:43:29.653706-04	5	Detalle object (5)	2	[{"changed": {"fields": ["Fecha de Encargo"]}}]	11	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	app	usuario
7	app	ingrediente
8	app	tipo
9	app	producto
10	app	encargo
11	app	detalle
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2022-07-05 12:54:50.330721-04
2	contenttypes	0002_remove_content_type_name	2022-07-05 12:54:50.344964-04
3	auth	0001_initial	2022-07-05 12:54:50.415887-04
4	auth	0002_alter_permission_name_max_length	2022-07-05 12:54:50.427949-04
5	auth	0003_alter_user_email_max_length	2022-07-05 12:54:50.442165-04
6	auth	0004_alter_user_username_opts	2022-07-05 12:54:50.458533-04
7	auth	0005_alter_user_last_login_null	2022-07-05 12:54:50.475448-04
8	auth	0006_require_contenttypes_0002	2022-07-05 12:54:50.49032-04
9	auth	0007_alter_validators_add_error_messages	2022-07-05 12:54:50.502593-04
10	auth	0008_alter_user_username_max_length	2022-07-05 12:54:50.515879-04
11	auth	0009_alter_user_last_name_max_length	2022-07-05 12:54:50.530624-04
12	auth	0010_alter_group_name_max_length	2022-07-05 12:54:50.544667-04
13	auth	0011_update_proxy_permissions	2022-07-05 12:54:50.557919-04
14	auth	0012_alter_user_first_name_max_length	2022-07-05 12:54:50.570618-04
15	app	0001_initial	2022-07-05 12:54:50.779126-04
16	admin	0001_initial	2022-07-05 12:54:50.831713-04
17	admin	0002_logentry_remove_auto_add	2022-07-05 12:54:50.853237-04
18	admin	0003_logentry_add_action_flag_choices	2022-07-05 12:54:50.872218-04
19	sessions	0001_initial	2022-07-05 12:54:50.889697-04
20	app	0002_remove_encargo_producto_encargo_producto	2022-07-05 14:56:19.269075-04
21	app	0003_alter_detalle_options_remove_encargo_cantidad_and_more	2022-07-05 16:05:18.060934-04
22	app	0004_alter_detalle_unique_together	2022-07-05 16:06:02.839697-04
23	app	0005_encargo_producto	2022-07-05 16:10:53.698962-04
24	app	0006_remove_encargo_fecha_encargo_detalle_fecha_encargo	2022-07-05 16:42:41.363798-04
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
umh6ykzsjsi05ci8t1lm8wxyboelmsqk	.eJxVjDsOwjAQBe_iGllLsNe7lPScIVr_cADZUpxUiLtDpBTQvpl5LzXKupRx7Wkep6jO6qQOv5uX8Eh1A_Eu9dZ0aHWZJ683Re-062uL6XnZ3b-DIr18a-sZnPGZhnwET5DFskcLJiZBQGYTCCUM4MDawMjBOJREkZgoZ1HvD9JdN5Y:1o8pOR:wR6A13G3kFGpDAR2asNbEMr_LwbZyayvB-OoxZ9I3cI	2022-07-19 16:43:35.681376-04
aezl1ugv7g3qx0bma6ayhx7gynw2qtkl	.eJxVjDsOwjAQBe_iGllLsNe7lPScIVr_cADZUpxUiLtDpBTQvpl5LzXKupRx7Wkep6jO6qQOv5uX8Eh1A_Eu9dZ0aHWZJ683Re-062uL6XnZ3b-DIr18a-sZnPGZhnwET5DFskcLJiZBQGYTCCUM4MDawMjBOJREkZgoZ1HvD9JdN5Y:1o8nrR:ZibPFAoRjXPPQmzPGEneQco47Ep7wZ_bZOIrw1XAUZc	2022-07-19 15:05:25.934315-04
a9wyz1slkdnbkelowwre3ratama7t7f6	.eJxVjDsOwjAQBe_iGllLsNe7lPScIVr_cADZUpxUiLtDpBTQvpl5LzXKupRx7Wkep6jO6qQOv5uX8Eh1A_Eu9dZ0aHWZJ683Re-062uL6XnZ3b-DIr18a-sZnPGZhnwET5DFskcLJiZBQGYTCCUM4MDawMjBOJREkZgoZ1HvD9JdN5Y:1o8nzO:Pe1oQhK5PfmcJ00r_ZiRXs4Xce7ZDnDTAaSsVzydVbg	2022-07-19 15:13:38.695192-04
\.


--
-- Name: app_detalle_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_detalle_id_seq', 6, true);


--
-- Name: app_encargo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_encargo_id_seq', 10, true);


--
-- Name: app_encargo_producto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_encargo_producto_id_seq', 3, true);


--
-- Name: app_ingrediente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_ingrediente_id_seq', 4, true);


--
-- Name: app_producto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_producto_id_seq', 2, true);


--
-- Name: app_producto_ingredientes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_producto_ingredientes_id_seq', 3, true);


--
-- Name: app_tipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_tipo_id_seq', 3, true);


--
-- Name: app_usuario_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_usuario_groups_id_seq', 2, true);


--
-- Name: app_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_usuario_id_seq', 4, true);


--
-- Name: app_usuario_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_usuario_user_permissions_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 2, true);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 44, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 32, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 11, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 24, true);


--
-- Name: app_detalle app_detalle_encargo_id_producto_id_0a2977ab_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_detalle
    ADD CONSTRAINT app_detalle_encargo_id_producto_id_0a2977ab_uniq UNIQUE (encargo_id, producto_id);


--
-- Name: app_detalle app_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_detalle
    ADD CONSTRAINT app_detalle_pkey PRIMARY KEY (id);


--
-- Name: app_encargo app_encargo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_encargo
    ADD CONSTRAINT app_encargo_pkey PRIMARY KEY (id);


--
-- Name: app_encargo_producto app_encargo_producto_encargo_id_producto_id_5482b5d6_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_encargo_producto
    ADD CONSTRAINT app_encargo_producto_encargo_id_producto_id_5482b5d6_uniq UNIQUE (encargo_id, producto_id);


--
-- Name: app_encargo_producto app_encargo_producto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_encargo_producto
    ADD CONSTRAINT app_encargo_producto_pkey PRIMARY KEY (id);


--
-- Name: app_ingrediente app_ingrediente_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_ingrediente
    ADD CONSTRAINT app_ingrediente_nombre_key UNIQUE (nombre);


--
-- Name: app_ingrediente app_ingrediente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_ingrediente
    ADD CONSTRAINT app_ingrediente_pkey PRIMARY KEY (id);


--
-- Name: app_producto_ingredientes app_producto_ingrediente_producto_id_ingrediente__b05360c9_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_producto_ingredientes
    ADD CONSTRAINT app_producto_ingrediente_producto_id_ingrediente__b05360c9_uniq UNIQUE (producto_id, ingrediente_id);


--
-- Name: app_producto_ingredientes app_producto_ingredientes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_producto_ingredientes
    ADD CONSTRAINT app_producto_ingredientes_pkey PRIMARY KEY (id);


--
-- Name: app_producto app_producto_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_producto
    ADD CONSTRAINT app_producto_nombre_key UNIQUE (nombre);


--
-- Name: app_producto app_producto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_producto
    ADD CONSTRAINT app_producto_pkey PRIMARY KEY (id);


--
-- Name: app_tipo app_tipo_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_tipo
    ADD CONSTRAINT app_tipo_nombre_key UNIQUE (nombre);


--
-- Name: app_tipo app_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_tipo
    ADD CONSTRAINT app_tipo_pkey PRIMARY KEY (id);


--
-- Name: app_usuario app_usuario_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario
    ADD CONSTRAINT app_usuario_email_key UNIQUE (email);


--
-- Name: app_usuario_groups app_usuario_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_groups
    ADD CONSTRAINT app_usuario_groups_pkey PRIMARY KEY (id);


--
-- Name: app_usuario_groups app_usuario_groups_usuario_id_group_id_0f4abc88_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_groups
    ADD CONSTRAINT app_usuario_groups_usuario_id_group_id_0f4abc88_uniq UNIQUE (usuario_id, group_id);


--
-- Name: app_usuario app_usuario_numero_telefono_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario
    ADD CONSTRAINT app_usuario_numero_telefono_key UNIQUE (numero_telefono);


--
-- Name: app_usuario app_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario
    ADD CONSTRAINT app_usuario_pkey PRIMARY KEY (id);


--
-- Name: app_usuario_user_permissions app_usuario_user_permiss_usuario_id_permission_id_6fd11793_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_user_permissions
    ADD CONSTRAINT app_usuario_user_permiss_usuario_id_permission_id_6fd11793_uniq UNIQUE (usuario_id, permission_id);


--
-- Name: app_usuario_user_permissions app_usuario_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_user_permissions
    ADD CONSTRAINT app_usuario_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: app_detalle_encargo_id_4a1aeedf; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_detalle_encargo_id_4a1aeedf ON public.app_detalle USING btree (encargo_id);


--
-- Name: app_detalle_producto_id_fe7c5058; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_detalle_producto_id_fe7c5058 ON public.app_detalle USING btree (producto_id);


--
-- Name: app_encargo_comprador_id_c24ccf6a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_encargo_comprador_id_c24ccf6a ON public.app_encargo USING btree (comprador_id);


--
-- Name: app_encargo_producto_encargo_id_63344be9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_encargo_producto_encargo_id_63344be9 ON public.app_encargo_producto USING btree (encargo_id);


--
-- Name: app_encargo_producto_producto_id_616f1eae; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_encargo_producto_producto_id_616f1eae ON public.app_encargo_producto USING btree (producto_id);


--
-- Name: app_ingrediente_nombre_958d4e19_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_ingrediente_nombre_958d4e19_like ON public.app_ingrediente USING btree (nombre varchar_pattern_ops);


--
-- Name: app_producto_ingredientes_ingrediente_id_27f0cdda; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_producto_ingredientes_ingrediente_id_27f0cdda ON public.app_producto_ingredientes USING btree (ingrediente_id);


--
-- Name: app_producto_ingredientes_producto_id_49006132; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_producto_ingredientes_producto_id_49006132 ON public.app_producto_ingredientes USING btree (producto_id);


--
-- Name: app_producto_nombre_57580f23_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_producto_nombre_57580f23_like ON public.app_producto USING btree (nombre varchar_pattern_ops);


--
-- Name: app_producto_tipo_id_617598ff; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_producto_tipo_id_617598ff ON public.app_producto USING btree (tipo_id);


--
-- Name: app_tipo_nombre_5b74d3ac_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_tipo_nombre_5b74d3ac_like ON public.app_tipo USING btree (nombre varchar_pattern_ops);


--
-- Name: app_usuario_email_0c530e1a_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_usuario_email_0c530e1a_like ON public.app_usuario USING btree (email varchar_pattern_ops);


--
-- Name: app_usuario_groups_group_id_b38b0d6e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_usuario_groups_group_id_b38b0d6e ON public.app_usuario_groups USING btree (group_id);


--
-- Name: app_usuario_groups_usuario_id_691971dd; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_usuario_groups_usuario_id_691971dd ON public.app_usuario_groups USING btree (usuario_id);


--
-- Name: app_usuario_user_permissions_permission_id_fbaf8fa8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_usuario_user_permissions_permission_id_fbaf8fa8 ON public.app_usuario_user_permissions USING btree (permission_id);


--
-- Name: app_usuario_user_permissions_usuario_id_d2c76ed5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_usuario_user_permissions_usuario_id_d2c76ed5 ON public.app_usuario_user_permissions USING btree (usuario_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: app_detalle app_detalle_encargo_id_4a1aeedf_fk_app_encargo_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_detalle
    ADD CONSTRAINT app_detalle_encargo_id_4a1aeedf_fk_app_encargo_id FOREIGN KEY (encargo_id) REFERENCES public.app_encargo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_detalle app_detalle_producto_id_fe7c5058_fk_app_producto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_detalle
    ADD CONSTRAINT app_detalle_producto_id_fe7c5058_fk_app_producto_id FOREIGN KEY (producto_id) REFERENCES public.app_producto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_encargo app_encargo_comprador_id_c24ccf6a_fk_app_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_encargo
    ADD CONSTRAINT app_encargo_comprador_id_c24ccf6a_fk_app_usuario_id FOREIGN KEY (comprador_id) REFERENCES public.app_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_encargo_producto app_encargo_producto_encargo_id_63344be9_fk_app_encargo_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_encargo_producto
    ADD CONSTRAINT app_encargo_producto_encargo_id_63344be9_fk_app_encargo_id FOREIGN KEY (encargo_id) REFERENCES public.app_encargo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_encargo_producto app_encargo_producto_producto_id_616f1eae_fk_app_producto_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_encargo_producto
    ADD CONSTRAINT app_encargo_producto_producto_id_616f1eae_fk_app_producto_id FOREIGN KEY (producto_id) REFERENCES public.app_producto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_producto_ingredientes app_producto_ingredi_ingrediente_id_27f0cdda_fk_app_ingre; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_producto_ingredientes
    ADD CONSTRAINT app_producto_ingredi_ingrediente_id_27f0cdda_fk_app_ingre FOREIGN KEY (ingrediente_id) REFERENCES public.app_ingrediente(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_producto_ingredientes app_producto_ingredi_producto_id_49006132_fk_app_produ; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_producto_ingredientes
    ADD CONSTRAINT app_producto_ingredi_producto_id_49006132_fk_app_produ FOREIGN KEY (producto_id) REFERENCES public.app_producto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_producto app_producto_tipo_id_617598ff_fk_app_tipo_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_producto
    ADD CONSTRAINT app_producto_tipo_id_617598ff_fk_app_tipo_id FOREIGN KEY (tipo_id) REFERENCES public.app_tipo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_usuario_groups app_usuario_groups_group_id_b38b0d6e_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_groups
    ADD CONSTRAINT app_usuario_groups_group_id_b38b0d6e_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_usuario_groups app_usuario_groups_usuario_id_691971dd_fk_app_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_groups
    ADD CONSTRAINT app_usuario_groups_usuario_id_691971dd_fk_app_usuario_id FOREIGN KEY (usuario_id) REFERENCES public.app_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_usuario_user_permissions app_usuario_user_per_permission_id_fbaf8fa8_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_user_permissions
    ADD CONSTRAINT app_usuario_user_per_permission_id_fbaf8fa8_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_usuario_user_permissions app_usuario_user_per_usuario_id_d2c76ed5_fk_app_usuar; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_usuario_user_permissions
    ADD CONSTRAINT app_usuario_user_per_usuario_id_d2c76ed5_fk_app_usuar FOREIGN KEY (usuario_id) REFERENCES public.app_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_app_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_app_usuario_id FOREIGN KEY (user_id) REFERENCES public.app_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

